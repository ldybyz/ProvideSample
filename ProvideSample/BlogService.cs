﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Linq;
using System.Text;
using System.Web.Configuration;

namespace ProvideSample
{
    public static class BlogService
    {
        #region Constants and Fields

        /// <summary>
        /// The lock object.
        /// </summary>
        private static readonly object TheLock = new object();

        /// <summary>
        /// The provider. Don't access this directly. Access it through the property accessor.
        /// </summary>
        private static BlogProvider _provider;

        /// <summary>
        /// the file storage provider. Don't access this directly. Access it the property accessor
        /// </summary>
        //private static BlogFileSystemProvider _fileStorageProvider;

        /// <summary>
        /// The providers.
        /// </summary>
        private static BlogProviderCollection _providers;


        //private static BlogFileSystemProviderCollection _fileProviders;
        #endregion

        #region Properties

        /// <summary>
        ///     gets the current FileSystem provider
        /// </summary>
        //public static BlogFileSystemProvider FileSystemProvider
        //{
        //    get
        //    {
        //        LoadProviders();
        //        return _fileStorageProvider;
        //    }
        //}

        /// <summary>
        ///     Gets a collection of FileSystem providers that are defined in Web.config.
        /// </summary>
        //public static BlogFileSystemProviderCollection FileSystemProviders
        //{
        //    get
        //    {
        //        LoadProviders();
        //        return _fileProviders;
        //    }
        //}

        /// <summary>
        ///     Gets the current provider.
        /// </summary>
        public static BlogProvider Provider
        {
            get
            {
                LoadProviders();
                return _provider;
            }
        }

        //internal static void ReloadFileSystemProvider()
        //{
        //    _fileStorageProvider = null;
        //    LoadProviders();
        //}

        /// <summary>
        ///     Gets a collection of all registered providers.
        /// </summary>
        public static BlogProviderCollection Providers
        {
            get
            {
                LoadProviders();
                return _providers;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// The fill blogs.
        /// </summary>
        /// <returns>
        /// A list of Blogs.
        /// </returns>
        public static string GetProviderName()
        {
            return Provider.GetProviderName();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Load the providers from the web.config.
        /// </summary>
        private static void LoadProviders()
        {
            // Avoid claiming lock if providers are already loaded
            if (_provider == null)
            {
                lock (TheLock)
                {
                    // Do this again to make sure _provider is still null
                    if (_provider == null)
                    {
                        // Get a reference to the <blogProvider> section
                        var section =
                            (BlogProviderSection)ConfigurationManager.GetSection("BlogEngine/blogProvider");

                        // Load registered providers and point _provider
                        // to the default provider
                        _providers = new BlogProviderCollection();
                        ProvidersHelper.InstantiateProviders(section.Providers, _providers, typeof(BlogProvider));
                        _provider = _providers[section.DefaultProvider];

                        if (_provider == null)
                        {
                            throw new ProviderException("Unable to load default BlogProvider");
                        }
                    }
                }
            }
            //if (_fileStorageProvider == null)
            //{
            //    lock (TheLock)
            //    {
            //        if (_fileStorageProvider == null)
            //        {
            //            var section = (BlogFileSystemProviderSection)WebConfigurationManager.GetSection("BlogEngine/blogFileSystemProvider");
            //            _fileProviders = new BlogFileSystemProviderCollection();
            //            ProvidersHelper.InstantiateProviders(section.Providers, _fileProviders, typeof(BlogFileSystemProvider));
            //            _fileStorageProvider = _fileProviders[section.DefaultProvider];
            //            if (_fileStorageProvider == null)
            //            {
            //                throw new ProviderException("unable to load default file system Blog Provider");
            //            }
            //        }
            //    }
            //}
        }

        #endregion
    }
}
