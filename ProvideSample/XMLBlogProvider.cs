﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Linq;
using System.Text;

namespace ProvideSample
{
    public partial class XMLBlogProvider:BlogProvider
    {
      


        public override string GetProviderName()
        {
            return "XMLBlogProvider";
        }
        /// <summary>
        /// Initializes the provider
        /// </summary>
        /// <param name="name">
        /// Configuration name
        /// </param>
        /// <param name="config">
        /// Configuration settings
        /// </param>
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config); 
        }
    }
}
